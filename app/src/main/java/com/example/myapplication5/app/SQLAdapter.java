package com.example.myapplication5.app;


// package com.example.myapplication2.app;

 import android.content.ContentValues;
 import android.content.Context;
 import android.database.Cursor;
 import android.database.sqlite.SQLiteDatabase;
 import android.util.Log;

 import java.sql.SQLException;

 public class SQLAdapter {

 // поля базы данных
 public static final String KEY_ROWID = "_id";
 public static final String KEY_NAME = "name";
 public static final String KEY_SURNAME = "surname";
 public static final String KEY_ADDS = "comm";
 private static final String DATABASE_TABLE = "main";
 private Context context;
 private SQLiteDatabase tmpDatabase;
 private SQLHelper tmpSQLHelper;

 public SQLAdapter(Context context) {
 this.context = context;
 }

 public SQLAdapter open() throws SQLException {
 tmpSQLHelper = new SQLHelper(context);
 tmpDatabase = tmpSQLHelper.getWritableDatabase();
 return this;
 }

 public void close() {
 tmpSQLHelper.close();
 }

 public long createItem(String name, String surname, String adds) {
 ContentValues initialValues = createContentValues(name, surname, adds);
 return tmpDatabase.insert(DATABASE_TABLE, null, initialValues);
 }

 public boolean updateItem(long rowId, String name, String surname,String adds) {
 ContentValues updateValues = createContentValues(name, surname, adds);
 return tmpDatabase.update(DATABASE_TABLE, updateValues, KEY_ROWID + "="+ rowId, null) > 0;
 }

 public boolean deleteItem(long rowId) {
 return tmpDatabase.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
 }

 public Cursor fetchAllItem() {
 Log.v("Cursor"," FetchAll");
 return tmpDatabase.query(DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME, KEY_SURNAME, KEY_ADDS }, null, null, null,null, null);
 }

 public Cursor fetchItem(long rowId) throws SQLException {
 Cursor mCursor = tmpDatabase.query(true, DATABASE_TABLE, new String[] {KEY_ROWID, KEY_NAME, KEY_SURNAME, KEY_ADDS},KEY_ROWID + "=" + rowId, null, null, null, null, null);
 if (mCursor != null) {
 mCursor.moveToFirst();
 }
 return mCursor;
 }

 private ContentValues createContentValues(String name, String surname, String adds) {
 ContentValues values = new ContentValues();
 values.put(KEY_NAME, name);
 values.put(KEY_SURNAME, surname);
 values.put(KEY_ADDS, adds);
 return values;
 }

 }
