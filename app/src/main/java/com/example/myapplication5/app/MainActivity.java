package com.example.myapplication5.app;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.ContextMenu;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
//import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.view.ViewGroup.LayoutParams;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
//import android.R;

import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.jar.Attributes;

public class MainActivity extends Activity {

    ActionMode actionMode;
    ListView lvActionMode;
    final static String LOG_TAG = "myLogs";


//    DBHelper dbHelper;

    String[] cr_students = { "Иванов Первый", "Петров Второй", "Сидоров Третий", "Иванов Четвертый", "Петров Пятый" };
    int[] students_id = {1, 2, 3, 4, 5 };

    String[] cr_groups = {"ИС-01", "ПС-11"};
    int[] groups_id = {1, 2};
    private static final String DATABASE_TABLE = "lessons";

    final String DROP_TABLE = "DROP TABLE IF EXISTS "+DATABASE_TABLE;


    static TextView tDate;
    View attView;
//    String tvDate;

    static ArrayList<String> students = new ArrayList<String>();
    static ArrayList<String> st_id = new ArrayList<String>();

    static ArrayList<String> groups = new ArrayList<String>();
    static ArrayList<String> gr_id = new ArrayList<String>();
    ArrayList<String> types = new ArrayList<String>();
    ArrayList<String> labs = new ArrayList<String>();
    ArrayList<String> grades = new ArrayList<String>();

    static ArrayList<String> lectures_rating = new ArrayList<String>();
    static ArrayList<String> lectures_att = new ArrayList<String>();
    static ArrayList<String> lectures_string = new ArrayList<String>();
    static ArrayList<String> practice_rating = new ArrayList<String>();
    static ArrayList<String> practice_att = new ArrayList<String>();
    static ArrayList<String> practice_string = new ArrayList<String>();
    static ArrayList<String> laboratory_rating = new ArrayList<String>();
    static ArrayList<String> laboratory_att = new ArrayList<String>();
    static ArrayList<String> laboratory_string = new ArrayList<String>();

    PopupWindow popUp;
    boolean click1 = false;
    boolean click2 = false;
    boolean click3 = false;
    boolean click4 = false;
    boolean click5 = false;

    TextView tv;
    Intent intent;
    static Context ctx;
    static SQLiteDatabase db;
    static DBHelper dbh;


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button group_stat_button = (Button) findViewById(R.id.buttonGroup);
        ctx = getApplicationContext();
        dbh = new DBHelper(ctx);
         db = initDB(dbh);

        group_stat_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(getApplicationContext(), Group_stat.class);
                String group_name = ((Spinner) findViewById(R.id.spinner)).getSelectedItem().toString();
                /*int pos = groups.indexOf(group_name);
                intent.putExtra("group_id",gr_id.get(pos));*/
                intent.putExtra("group_name",group_name);
                startActivity(intent);
            }
        });




        /*textDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(1);
            }
        });*/

        /*DBHelper dbh = new DBHelper(getApplicationContext());
        SQLiteDatabase db = initDB(dbh);*/


//        setSpinner(db,dbh);
        setSpinner();
//        setStudentsList();




//        lvActionMode.setOnItemLongClickListener(new OnItemLongClickListener() {


/*        lvActionMode.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int pos, long arg3) {
                actionMode = startActionMode(new ActionModeCallback());
                //actionMode = MainActivity.this.startActionMode(onCreateActionMode(actionMode));
                return true;
            }


        });*/


    }
    //создание контекстного меню
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Выберите свойство");
        menu.add(0, 0, 0, R.string.progress);
//        menu.add(0, 1, 0, R.string.attendance);
        menu.add(0, 1, 0, R.string.stat);
    }



    public class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            /*// Do something with the date chosen by the user
            LayoutInflater inflater = LayoutInflater.from(getApplication());
            final View builderView = inflater.inflate(R.layout.prog_dialog, null);

//            View attView = LayoutInflater.from(getApplication()).inflate(R.layout.prog_dialog, null);
            TextView tDate = (TextView) builderView.findViewById(R.id.tvDate);*/
            int inc_month = month+1;
//            tDate.invalidate();
            String result_s = day + "." + inc_month + "." + year;
            Log.d(LOG_TAG, "RESULT text " + result_s);
            Log.d(LOG_TAG, "TVDATE text before " + tDate.getText().toString());
            tDate.setText(result_s);
            Log.d(LOG_TAG, "TVDATE text after " + tDate.getText().toString());
//            builderView.invalidate();
//            tDate.
//            tDate.();
        }
    }

   /* public void showDatePickerDialog(View v) {
        Log.d(LOG_TAG,"CLICKED");
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(),"datePicker");
//        newFragment.show(getSupportFragmentManager(), "datePicker");
    }*/



   /* protected Dialog onCreateDialog(int id) {
        if (id == 1) {
            DatePickerDialog tpd = new DatePickerDialog(this, myCallBack, myYear, myMonth, myDay);
            return tpd;
        }
        return super.onCreateDialog(id);
    }*/

    /*DatePickerDialog.OnDateSetListener myCallBack = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            TextView textDate = (TextView) findViewById(R.id.tvDate);
            myYear = year;
            myMonth = monthOfYear;
            myDay = dayOfMonth;
            textDate.setText("Today is " + myDay + "/" + myMonth + "/" + myYear);
        }
    };*/

    //выбор контекстного меню
    public boolean onContextItemSelected(MenuItem item) {
        /*if (item.getItemId() == CM_DELETE_ID) {
            // получаем из пункта контекстного меню данные по пункту списка, надо уточнять как работает
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            // извлекаем id записи и удаляем соответствующую запись в БД
            dbHelper.deleteItem(acmi.id) ;
            // получаем новый курсор с данными
            getLoaderManager().getLoader(0).forceLoad();
            return true;
        }*/
        AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        String position = Integer.toString(acmi.position);
        if (item.getItemId() == 0) {
            Log.d(LOG_TAG,"POSITION"+position);
            openProgDialog(position);

        } else {
            if (item.getItemId() == 1) {
//                openAttDialog(position);
                openStatDialog(position);

            }
        }
        return super.onContextItemSelected(item);
    }
    /*public static XmlPullParser prepareXpp() {
        return this.getContext().getResources().getXml(R.xml.data);
    }*/

    private void setStudentsList(String gr_id) {//,SQLiteDatabase db, DBHelper dbh){
        //ArrayList<String> data2 = getDataFromXML("students","ПС-11");
//        SQLiteDatabase db = initDB();
//        ArrayList<String> data3 = workWithDB(1,gr_id,db,dbh);
        ArrayList<String> data3 = workWithDB(1,gr_id,0,null);
        st_id.clear();
        students.clear();


        for (int i = 1; i< data3.size()+1; i++) {
            if ((i%2)!=0) {
                st_id.add(data3.get(i-1));
            } else {
                students.add(data3.get(i-1));
            }
        }
        //Log.d(LOG_TAG,"DATA"+data2);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, students);
        lvActionMode = (ListView) findViewById(R.id.lvActionMode);
        lvActionMode.setAdapter(adapter);
//        lvActionMode.setChoiceMode(ListView.CHOICE_MODE_SINGLE);

        /*lvActionMode.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                actionMode = startActionMode(new ActionModeCallback());
            }
        });*/
        registerForContextMenu(lvActionMode);

    }

//    private void setSpinner(SQLiteDatabase db, DBHelper dbh) {
    private void setSpinner() {

        types.add("Лекция");
        types.add("Практика");
        types.add("Лабораторная");

        for (int i = 1; i <6; i++) {
            labs.add(Integer.toString(i));
        }

        for (int i = 0; i <=100; i++) {
            grades.add(Integer.toString(i));
        }


        gr_id.clear();
        groups.clear();
//        SQLiteDatabase db = initDB();
//        ArrayList<String> data = workWithDB(2,null,db,dbh);
        ArrayList<String> data = workWithDB(2,null,0,null);
        for (int i = 1; i< data.size()+1; i++) {
            if ((i%2)!=0) {
                gr_id.add(data.get(i-1));
            } else {
                groups.add(data.get(i-1));
            }
        }

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,groups);
        spinner.setAdapter(adapter); // this will set list of values to spinner

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                /*DBHelper dbh = new DBHelper(getApplicationContext());
                SQLiteDatabase db = dbh.getWritableDatabase();*/

                /*if (actionMode != null)
                {
                    actionMode.finish();
                }*/
                // показываем позиция нажатого элемента
                String group_name = ((Spinner) findViewById(R.id.spinner)).getSelectedItem().toString();
                int pos = groups.indexOf(group_name);
//                setStudentsList(gr_id.get(pos),db,dbh);
                setStudentsList(gr_id.get(pos));
                rating();
                //Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }

//        spinner.setSelection(0);//set selected value in spinner


        });

    }



   /* private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            mode.getMenuInflater().inflate(R.menu.context, menu);
            return true;
        }
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
//                Toast toast = Toast.makeText(getApplicationContext(), item.getTitle(),Toast.LENGTH_SHORT);
//            openAttDialog();
            int checked = lvActionMode.getCheckedItemPosition();
            String checked_item = "";
            checked_item = lvActionMode.getItemAtPosition(checked).toString();

            switch (item.getItemId()) {
                case R.id.item1:
                    openAttDialog(checked_item);
                    return true;
                case R.id.item2:
                    openProgDialog(checked_item);
                    return true;
                default:
                    return false;
            }
            *//*int checked = lvActionMode.getCheckedItemPosition();
            String checked_items = "";
            checked_items = lvActionMode.getItemAtPosition(checked).toString();
                *//**//*for (int i = 0; i < checked.size(); i++) {
                    if (checked.get(i) == true) {
                        checked_items += lvActionMode.getItemAtPosition(i)+"\n";
                    }
                }*//**//*
            Toast toast = Toast.makeText(getApplicationContext(), checked_items, Toast.LENGTH_SHORT);
            toast.show();*//*
            //mode.finish();
//            return false;
        }
        @Override
        public void onDestroyActionMode(ActionMode mode) {
        }

        *//*public void onItemCheckedStateChanged(ActionMode mode,
                                              int position, long id, boolean checked) {
            Log.d(LOG_TAG, "position = " + position + ", checked = "
                    + checked);
        }*//*
    }*/

    private void openProgDialog(String checked_item) {
        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        String student_name = students.get(Integer.parseInt(checked_item));
        alert.setTitle(student_name);

        LayoutInflater inflater = LayoutInflater.from(getApplication());
        attView = inflater.inflate(R.layout.prog_dialog, null);

        tDate = (TextView) attView.findViewById(R.id.tvDate);
        Calendar c = Calendar.getInstance();
        int Year = c.get(Calendar.YEAR);
        int Month = c.get(Calendar.MONTH)+1;
        int Day = c.get(Calendar.DAY_OF_MONTH);

        tDate.setText(Day + "." + Month + "." + Year);
        tDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(LOG_TAG,"CLICKED");
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getFragmentManager(),"datePicker");
//        newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });

//        LayoutInflater inflater = LayoutInflater.from(this);
//        final View builderView = inflater.inflate(R.layout.prog_dialog, null);

//        ((FrameLayout)attView.getParent()).removeView(attView);
//        ((FrameLayout)attView.getParent()).removeView(attView);
        /*LayoutInflater inflater = LayoutInflater.from(getApplication());
        View attView = inflater.inflate(R.layout.prog_dialog, null);*/
        final View builderView = attView;

//        TextView tDate = (TextView) builderView.findViewById(R.id.tvDate);


//        final DatePicker datePicker = (DatePicker) builderView.findViewById(R.id.AlertDatePicker);
        final TextView labelN = (TextView) builderView.findViewById(R.id.textView2);
        final TextView labelG = (TextView) builderView.findViewById(R.id.textView);
        final Spinner spinnerN = (Spinner) builderView.findViewById(R.id.AlertSpinnerN);//number of lab
        final Spinner spinnerT = (Spinner) builderView.findViewById(R.id.AlertSpinnerT);//type of lesson
        final Spinner spinnerG = (Spinner) builderView.findViewById(R.id.AlertSpinnerG);//grade
        final CheckBox checkAbs = (CheckBox) builderView.findViewById(R.id.AlertCheckBoxAbs);
        final EditText edt= (EditText) builderView.findViewById(R.id.AlertEditText);
        final EditText edtN= (EditText) builderView.findViewById(R.id.AlertNumberText);//!!!!!!!!!!!!!!!!!
        final EditText edtDate= (EditText) builderView.findViewById(R.id.AlertDateText);///!!!!!!!!!!!!!!!!

//        final RelativeLayout r1 = (RelativeLayout) builderView.findViewById(R.id.r1);
        final RelativeLayout.LayoutParams p = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
//        final RelativeLayout.LayoutParams p2 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        final RelativeLayout r2 = (RelativeLayout) builderView.findViewById(R.id.type2);

        final CheckBox checkP = (CheckBox) builderView.findViewById(R.id.AlertCheckBoxP);
        final CheckBox checkT = (CheckBox) builderView.findViewById(R.id.AlertCheckBoxT);
        final CheckBox checkO = (CheckBox) builderView.findViewById(R.id.AlertCheckBoxO);
//        int pos = students.indexOf(checked_item);
//        final String student_id = st_id.get(pos);
        final String student_id = st_id.get(Integer.parseInt(checked_item));



        /*edt.setVisibility(View.GONE);
        edtN.setVisibility(View.GONE);
        edtDate.setVisibility(View.GONE);*/
//        checkBox.setVisibility(View.GONE);
        //set number of grades
        ArrayAdapter<String> adapterG = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,grades);
        spinnerG.setAdapter(adapterG); // this will set list of values to spinner
        //set number of labs
        ArrayAdapter<String> adapterL = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,labs);
        spinnerN.setAdapter(adapterL); // this will set list of values to spinner
        //choose type
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item,types);
        spinnerT.setAdapter(adapter); // this will set list of values to spinner
        p.addRule(RelativeLayout.BELOW, R.id.type2);
//        p2.addRule(RelativeLayout.BELOW, R.id.AlertSpinnerG);
        labelG.setVisibility(View.INVISIBLE);
        spinnerG.setVisibility(View.INVISIBLE);
        labelN.setVisibility(View.INVISIBLE);
        spinnerN.setVisibility(View.INVISIBLE);
        r2.setVisibility(View.GONE);

        tDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (spinnerT.getSelectedItemPosition()!= 2) {
                    int position = spinnerT.getSelectedItemPosition();
                    boolean checked=false;
                    String date = tDate.getText().toString();
                    ArrayList<String> this_lesson = new ArrayList<String>();
                    this_lesson = workWithDB(3, student_id, position+1,date);
//                                {"id,date,type,number,practice,theory,report,grade,note"}
                    if (!this_lesson.isEmpty()) {
                        Log.d(LOG_TAG,"OnChangeDate = "+ this_lesson.toString());
                        int grade = Integer.parseInt(this_lesson.get(7));
                        spinnerG.setSelection(grade);
                        edt.setText(this_lesson.get(8));
                        switch (Integer.parseInt(this_lesson.get(9))) {
                            case 0:
                                checked = false;
                                break;
                            case 1:
                                checked = true;
                                break;
                        }
                        checkAbs.setChecked(checked);
                    }

                    else {
                        if (position==2) {
                            checkP.setChecked(false);
                            checkT.setChecked(false);
                            checkO.setChecked(false);
                            checkAbs.setChecked(false);
                        }
                        spinnerG.setSelection(0);
                        checkAbs.setChecked(false);
                        edt.setText("");
                    }
                }

            }
        });

        spinnerT.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                      final int position, long id) {
                String date = tDate.getText().toString();
                /*checkT.setVisibility(View.GONE);
                checkP.setVisibility(View.GONE);
                checkO.setVisibility(View.GONE);
                spinnerN.setVisibility(View.GONE);*/
//                r1.setVisibility(View.GONE);
                boolean checked = false;
                labelG.setVisibility(View.INVISIBLE);
                spinnerG.setVisibility(View.INVISIBLE);
                labelN.setVisibility(View.INVISIBLE);
                spinnerN.setVisibility(View.INVISIBLE);
                r2.setVisibility(View.GONE);
                ArrayList<String> this_lesson = new ArrayList<String>();


                switch (position) {
                    case 0://лекция
                        this_lesson.clear();
                        this_lesson = workWithDB(3, student_id, position+1,date);
//                                {"id,date,type,number,practice,theory,report,grade,note"}
                        if (!this_lesson.isEmpty()) {
                            Log.d(LOG_TAG,"Lect = "+ this_lesson.toString());
                            int grade = Integer.parseInt(this_lesson.get(7));
                            spinnerG.setSelection(grade);
                            edt.setText(this_lesson.get(8));
                            switch (Integer.parseInt(this_lesson.get(9))) {
                                case 0:
                                    checked = false;
                                    break;
                                case 1:
                                    checked = true;
                                    break;
                            }
                            checkAbs.setChecked(checked);
                        }

                        else {
                            spinnerG.setSelection(0);
                            edt.setText("");
                            checkAbs.setChecked(false);
                        }
                       break;
                    case 1://практика
                        this_lesson.clear();
                        this_lesson = workWithDB(3, student_id, position+1,date);
//                                {"id,date,type,number,practice,theory,report,grade,note"}
                        if (!this_lesson.isEmpty()) {
                            Log.d(LOG_TAG,"Prac = "+ this_lesson.toString());
                            int grade = Integer.parseInt(this_lesson.get(7));
                            labelG.setVisibility(View.VISIBLE);
                            spinnerG.setVisibility(View.VISIBLE);
                            spinnerG.setSelection(grade);
                            edt.setText(this_lesson.get(8));
                            switch (Integer.parseInt(this_lesson.get(9))) {
                                case 0:
                                    checked = false;
                                    break;
                                case 1:
                                    checked = true;
                                    break;
                            }
                            checkAbs.setChecked(checked);
                        }

                        else {
                            labelG.setVisibility(View.VISIBLE);
                            spinnerG.setVisibility(View.VISIBLE);
                            spinnerG.setSelection(0);
                            edt.setText("");
                            checkAbs.setChecked(false);
                        }
                    break;
                    case 2://лабораторная
                        this_lesson.clear();
                        int position_n = spinnerN.getSelectedItemPosition();
                        this_lesson = workWithDB(3, student_id, position+1,Integer.toString(position_n+1));
//                                {"id,date,type,number,practice,theory,report,grade,note"}
                        if (!this_lesson.isEmpty()) {
//                                    otch = (checkO.isChecked()) ? 1: 0;

                            Log.d(LOG_TAG,"LaB = "+ this_lesson.toString());
                            int grade = Integer.parseInt(this_lesson.get(7));
                            tDate.setText(this_lesson.get(1));
//                            boolean checked = false;
                            checked = false;
                            switch (Integer.parseInt(this_lesson.get(4))) {
                                case 0:
                                    checked = false;
                                    break;
                                case 1:
                                    checked = true;
                                    break;
                            }
                            checkP.setChecked(checked);
                            switch (Integer.parseInt(this_lesson.get(5))) {
                                case 0:
                                    checked = false;
                                    break;
                                case 1:
                                    checked = true;
                                    break;
                            }
                            checkT.setChecked(checked);

                            switch (Integer.parseInt(this_lesson.get(6))) {
                                case 0:
                                    checked = false;
                                    break;
                                case 1:
                                    checked = true;
                                    break;
                            }
                            checkO.setChecked(checked);
                            switch (Integer.parseInt(this_lesson.get(9))) {
                                case 0:
                                    checked = false;
                                    break;
                                case 1:
                                    checked = true;
                                    break;
                            }
                            checkAbs.setChecked(checked);
                            labelG.setVisibility(View.VISIBLE);
                            spinnerG.setVisibility(View.VISIBLE);
                            spinnerG.setSelection(grade);
                            edt.setText(this_lesson.get(8));
                            //заполнить все поля формы из this_lesson
                        }

                        else {
                            labelG.setVisibility(View.VISIBLE);
                            spinnerG.setVisibility(View.VISIBLE);
                            spinnerG.setSelection(0);
                            checkP.setChecked(false);
                            checkT.setChecked(false);
                            checkO.setChecked(false);
                            checkAbs.setChecked(false);
                            edt.setText("");
                        }
                        /*checkT.setVisibility(View.VISIBLE);
                        checkP.setVisibility(View.VISIBLE);
                        checkO.setVisibility(View.VISIBLE);
                        spinnerN.setVisibility(View.VISIBLE);*/

                        /*edt.setVisibility(View.GONE);
                        edtN.setVisibility(View.GONE);
//                        edtDate.setVisibility(View.INVISIBLE);
                        edtDate.setVisibility(View.VISIBLE);
                        alert.setView(builderView);*/

                        spinnerN.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view,
                                                       int position_num, long id) {
                                ArrayList<String> this_lesson = new ArrayList<String>();
                                this_lesson = workWithDB(3, student_id, position+1,Integer.toString(position_num+1));
//                                {"id,date,type,number,practice,theory,report,grade,note"}
                                if (!this_lesson.isEmpty()) {
//                                    otch = (checkO.isChecked()) ? 1: 0;

                                    Log.d(LOG_TAG,"THIS lesson = "+ this_lesson.toString());
                                    int grade = Integer.parseInt(this_lesson.get(7));
                                    tDate.setText(this_lesson.get(1));
                                    boolean checked = false;
                                    switch (Integer.parseInt(this_lesson.get(4))) {
                                        case 0:
                                            checked = false;
                                        break;
                                        case 1:
                                            checked = true;
                                        break;
                                    }
                                    checkP.setChecked(checked);
                                    switch (Integer.parseInt(this_lesson.get(5))) {
                                        case 0:
                                            checked = false;
                                            break;
                                        case 1:
                                            checked = true;
                                            break;
                                    }
                                    checkT.setChecked(checked);

                                    switch (Integer.parseInt(this_lesson.get(6))) {
                                        case 0:
                                            checked = false;
                                            break;
                                        case 1:
                                            checked = true;
                                            break;
                                    }
                                    checkO.setChecked(checked);
                                    switch (Integer.parseInt(this_lesson.get(9))) {
                                        case 0:
                                            checked = false;
                                            break;
                                        case 1:
                                            checked = true;
                                            break;
                                    }
                                    checkAbs.setChecked(checked);
                                    spinnerG.setSelection(grade);
                                    edt.setText(this_lesson.get(8));
                                //заполнить все поля формы из this_lesson
                                }

                                else {
                                    spinnerG.setSelection(0);
                                    checkP.setChecked(false);
                                    checkT.setChecked(false);
                                    checkO.setChecked(false);
                                    checkAbs.setChecked(false);
                                    edt.setText("");
                                }
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> arg0) {
                            }

                        });
                        labelN.setVisibility(View.VISIBLE);
                        spinnerN.setVisibility(View.VISIBLE);
//                        checkAbs.setLayoutParams(p2);
                        r2.setVisibility(View.VISIBLE);
                        edt.setLayoutParams(p);
                    break;
                }
                /*String group_name = ((Spinner) findViewById(R.id.spinner)).getSelectedItem().toString();
                int pos = groups.indexOf(group_name);
//                setStudentsList(gr_id.get(pos),db,dbh);
                setStudentsList(gr_id.get(pos));*/
                //Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }

//        spinner.setSelection(0);//set selected value in spinner


        });
        /////
        //Тут должен быть подгружен результат запроса к БД, передать id студента, тип занятия "3" и номер лабораторной.
        //Полученными данными заполнить все поля в создаваемой форме.

        edt.setHint("Примечание");
        edtN.setHint("0 баллов");

        /*final Calendar c = Calendar.getInstance();
        int yyyy = c.get(Calendar.YEAR);
        int MM = c.get(Calendar.MONTH);
        int dd = c.get(Calendar.DAY_OF_MONTH);

        edtDate.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(dd).append(".").append(MM + 1).append(".")
                .append(yyyy));*/

        alert.setView(builderView);

        alert.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = "";
                String date = "";
                String type = "";
                String number = "";
                int type_n=0;
                int prac=0;
                int teor=0;
                int otch=0;
                int abs=0;
//                value = edtN.getText().toString();
                value = spinnerG.getSelectedItem().toString();
                date = tDate.getText().toString();
                type = spinnerT.getSelectedItem().toString();
                abs = (checkAbs.isChecked()) ? 1: 0;
                if (type.equals("Лекция")) {
                    type_n = 1;
                } else if (type.equals("Практика")){
                    type_n = 2;
                } else if (type.equals("Лабораторная")) {
                    type_n = 3;
                }
                if (type.equals("Лабораторная")){
                    number = spinnerN.getSelectedItem().toString();
                    prac = (checkP.isChecked()) ? 1: 0;
                    teor = (checkT.isChecked()) ? 1: 0;
                    otch = (checkO.isChecked()) ? 1: 0;


                    ArrayList<String> this_lesson = new ArrayList<String>();
                    this_lesson = workWithDB(3, student_id, type_n,number);
                    if (!this_lesson.isEmpty()) {
                        String id_field = this_lesson.get(0);
                        writeToDB(2,type_n,Integer.parseInt(number),prac,teor,otch,date,abs,Integer.parseInt(value),Integer.parseInt(student_id),edt.getText().toString(),id_field);
                        Toast toast = Toast.makeText(getApplicationContext(), "Обновлено", Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        writeToDB(0,type_n,Integer.parseInt(number),prac,teor,otch,date,abs,Integer.parseInt(value),Integer.parseInt(student_id),edt.getText().toString(),null);
                        Toast toast = Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                } else {
                    ArrayList<String> this_lesson = new ArrayList<String>();
                    this_lesson = workWithDB(3, student_id, type_n,date);
                    if (!this_lesson.isEmpty()) {
                        String id_field = this_lesson.get(0);
                        writeToDB(2,type_n,0,2,2,2,date,abs,Integer.parseInt(value),Integer.parseInt(student_id),edt.getText().toString(),id_field);
                        Toast toast = Toast.makeText(getApplicationContext(), "Обновлено", Toast.LENGTH_SHORT);
                        toast.show();
                    } else {
                        writeToDB(0,type_n,0,2,2,2,date,abs,Integer.parseInt(value),Integer.parseInt(student_id),edt.getText().toString(),null);
                        Toast toast = Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_SHORT);
                        toast.show();
                    }
                }



//                if (value.isEmpty() == false) {


                    /*Toast toast = Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT);
                    toast.show();*/
//                } else {
                    /*Toast toast = Toast.makeText(getApplicationContext(), "Не сохранено: не указано количество баллов", Toast.LENGTH_LONG);
                    toast.show();*/
//                }

//                ((ViewGroup)attView.getParent()).removeView(attView);
                /*spinnerT.setSelection(0);
                spinnerN.setSelection(0);*/
                ((FrameLayout)builderView.getParent()).removeView(builderView);
                /*attView.forceLayout();
                attView.destroyDrawingCache();*/

                // Получили значение введенных данных!
            }
        });

        alert.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Если отменили.
//                ((FrameLayout)attView.getParent()).removeView(attView);
                ((FrameLayout)builderView.getParent()).removeView(builderView);
                /*attView.forceLayout();
                attView.destroyDrawingCache();*/
            }
        });

        alert.show();
    }

    private void rating() {
        lectures_att.clear();
        lectures_rating.clear();
        lectures_string.clear();
        practice_rating.clear();
        practice_att.clear();
        practice_string.clear();
        laboratory_rating.clear();
        laboratory_att.clear();
        practice_att.clear();
        ArrayList<String> temp = new ArrayList<String>();
        ArrayList<String> lectures_id = new ArrayList<String>();
        String student_id = "";
        int j;
        int jj = 0;
        int summ_grade=0;
        int average=0,attendance;
        double average_d,attendance_d;

        for (int i = 0; i<students.size();i++) {
//            summ_grade = 0;

            student_id = st_id.get(i);
            lectures_id = workWithDB(4,student_id,1,null);
            j = lectures_id.size();
            jj = 0;
            if (j != 0) {
                for (int k = 0; k<j;k++) {
                    temp = workWithDB(5,student_id,0,lectures_id.get(k));
                    if (Integer.parseInt(temp.get(1)) == 0) {
    //                    lectures.add(temp.get(0));
                    } else {
                        jj++;
                    }
    //                summ_grade += Integer.parseInt(temp.get(2));
                    if (k==(j-1)) {
    //                    average_d = summ_grade/(double)j;
    //                    average = (int)Math.round(average_d);
                        lectures_rating.add("0");
                        lectures_string.add(jj+"/"+j);
                        attendance_d = (jj/(double)j)*100;
                        attendance = (int)Math.round(attendance_d);
                        lectures_att.add(Integer.toString(attendance));
                    }
                }
            } else {
                lectures_rating.add("0");
                lectures_string.add("0/0");
                lectures_att.add("0");
            }

            temp.clear();
            lectures_id.clear();
            summ_grade = 0;
            jj = 0;

            lectures_id = workWithDB(4,student_id,2,null);
            j = lectures_id.size();
            if (j != 0) {
                for (int k = 0; k<j;k++) {
                    temp = workWithDB(5,student_id,0,lectures_id.get(k));
                    if (Integer.parseInt(temp.get(1)) == 0) {
    //                    lectures.add(temp.get(0));
                    } else {
                        jj++;
                    }
                    summ_grade += Integer.parseInt(temp.get(2));
                    if (k==(j-1)) {
                        average_d = summ_grade/(double)j;
                        average = (int)Math.round(average_d);
                        practice_rating.add(Integer.toString(average));
                        practice_string.add(jj+"/"+j);
                        attendance_d = (jj/(double)j)*100;
                        attendance = (int)Math.round(attendance_d);
                        practice_att.add(Integer.toString(attendance));
                    }
                }
            } else {
                practice_rating.add("0");
                practice_string.add("0/0");
                practice_att.add("0");
            }

            temp.clear();
            lectures_id.clear();
            summ_grade = 0;
            jj = 0;

            lectures_id = workWithDB(4,student_id,3,null);
            j = lectures_id.size();
            if (j != 0) {
                for (int k = 0; k<j;k++) {
                    temp = workWithDB(5,student_id,0,lectures_id.get(k));
                    if (Integer.parseInt(temp.get(1)) == 0) {
    //                    lectures.add(temp.get(0));
                    } else {
                        jj++;
                    }
                    summ_grade += Integer.parseInt(temp.get(2));
                    if (k==(j-1)) {
                        average_d = summ_grade/(double)j;
                        average = (int)Math.round(average_d);
                        laboratory_rating.add(Integer.toString(average));
                        laboratory_string.add(jj+"/"+j);
                        attendance_d = (jj/(double)j)*100;
                        attendance = (int)Math.round(attendance_d);
                        laboratory_att.add(Integer.toString(attendance));
                    }
                }
            } else {
                laboratory_rating.add("0");
                laboratory_string.add("0/0");
                laboratory_att.add("0");
            }
        }
        /*for (int k = 0; k<j;k++) {
            temp = workWithDB(5,student_id,0,lectures_id.get(k));
            //{"date,is_visit,grade,number,practice,theory,report"}
            if (Integer.parseInt(temp.get(1)) == 0) {
                lectures.add(temp.get(0));
            } else {
                jj++;
            }

            temp_int = 6-(temp.get(0).length()-9)*2;
//            Log.d(LOG_TAG,"TEMP INT = "+temp_int);
            for (int i = 1; i<=temp_int;i++) {
                temp_string += " ";
            }
            values.add(temp.get(0)+temp_string+temp.get(2));
            temp_string = "";
            summ_grade += Integer.parseInt(temp.get(2));
            if (k==(j-1)) {
                average_d = summ_grade/(double)j;
                average = (int)Math.round(average_d);
            }*/
        Log.d(LOG_TAG,"LECT_att"+lectures_att.toString());
        Log.d(LOG_TAG,"Prac_att"+practice_att.toString());
        Log.d(LOG_TAG,"Lab_att"+laboratory_att.toString());

        Log.d(LOG_TAG,"Prac_rate"+practice_rating.toString());
        Log.d(LOG_TAG,"Lab_rate"+laboratory_rating.toString());
    }

    private void openStatDialog(String checked_item) {
        /*Rect displayRectangle = new Rect();
        Window window = this.getWindow();
        window.getDecorView().getWindowVisibleDisplayFrame(displayRectangle);
        LayoutInflater inflater = LayoutInflater.from(this);
        View builderView = inflater.inflate(R.layout.stat_dialog, null);
        builderView.setMinimumWidth((int) (displayRectangle.width() * 0.9f));
        builderView.setMinimumHeight((int) (displayRectangle.height() * 0.9f));*/
       // AlertDialog.Builder builder = new AlertDialog.Builder(this);
        //builder.setView(builderView);
        //AlertDialog alert = builder.create();
        //alert.show();

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        String student_name = students.get(Integer.parseInt(checked_item));
        final String student_id = st_id.get(Integer.parseInt(checked_item));
        alert.setTitle(student_name);

        LayoutInflater inflater = LayoutInflater.from(getApplication());
//        attView = inflater.inflate(R.layout.stat_dialog, null);

//        if (checked_item.isEmpty()==false) {

//        checkBox.setText("Присутствие на занятии");
//        AlertDialog.Builder alert = new AlertDialog.Builder(this);
//        alert.setView(builderView);
        //---------checked_item - элемент в списке
//        alert.setView(checkBoxView);
        //alert.setMessage("Введите количество баллов");
        // Добавим поле ввода
        //final CheckBox input = new CheckBox(this);
//        input.setText("Присутствие на занятии");
//        LayoutInflater inflater = LayoutInflater.from(this);
        final View builderView = inflater.inflate(R.layout.stat_dialog, null);
//        final View mainLayout = inflater.inflate(R.layout.activity_main, null);
        /*final CheckBox chb;
        final EditText edt= (EditText) builderView.findViewById(R.id.AlertEditText);
        final EditText edtDate= (EditText) builderView.findViewById(R.id.AlertDateText);
        chb = (CheckBox) builderView.findViewById(R.id.AlertCheckBox);
        edt.setHint("Примечание");
        chb.setText("Присутствовал(а)");
//        int pos = students.indexOf(checked_item);
        final String student_id = st_id.get(Integer.parseInt(checked_item));
//        edt.setText(student_id);
        final Calendar c = Calendar.getInstance();
        int yyyy = c.get(Calendar.YEAR);
        int MM = c.get(Calendar.MONTH);
        int dd = c.get(Calendar.DAY_OF_MONTH);
        String format = "dd.mm.yyyy";
*/
        /*SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.getDefault());
        System.err.format("%30s %s\n", format, sdf.format(new Date(0)));
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        System.err.format("%30s %s\n", format, sdf.format(new Date(0)));*/

        // set current date into textview
//        String blabla = sdf.format(format);
//        edtDate.setText(blabla);
        /*edtDate.setText(new StringBuilder()
                // Month is 0 based, just add 1
                .append(dd).append(".").append(MM + 1).append(".")
                .append(yyyy));*/

        final TextView textL = (TextView) builderView.findViewById(R.id.textViewL);
        final TextView textPracP = (TextView) builderView.findViewById(R.id.textViewPracP);
        final TextView textPracU = (TextView) builderView.findViewById(R.id.textViewPracU);
        final TextView textLabP = (TextView) builderView.findViewById(R.id.textViewLabP);
        final TextView textLabU = (TextView) builderView.findViewById(R.id.textViewLabU);
//        LinearLayout popup_l = new LinearLayout(textL.getContext());
//        popup_l.setBackgroundColor(Color.BLUE);
        popUp = new PopupWindow();

//        popUp = new PopupWindow(popup_l, LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
//        popup_l.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d(LOG_TAG,"CLICK!!!");
//            }
//        });
        /*final LinearLayout layout;
        layout = new LinearLayout(this);
        tv = new TextView(this);
        LayoutParams params;
        LayoutParams params_layout;

        params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);
        params_layout = new LayoutParams(LayoutParams.FILL_PARENT,LayoutParams.WRAP_CONTENT);
        *//*params_layout.width = 200;
        params_layout.height = 50;*//*
        layout.setOrientation(LinearLayout.VERTICAL);
//        layout.setLayoutParams(params);
        tv.setText("Hi this is a sample text for popup window");
        *//*tv.setHeight(40);
        tv.setWidth(150);*//*
        layout.addView(tv, params);
        layout.setLayoutParams(params_layout);
        layout.setBackgroundColor(Color.BLUE);
        popUp.setContentView(layout);
        Log.d(LOG_TAG,"Layout params "+layout.getWidth()+" "+layout.getHeight());*/
        ArrayList<String> lectures_id = new ArrayList<String>();
        ArrayList<String> lectures = new ArrayList<String>();
        ArrayList<String> temp = new ArrayList<String>();
        int j = 0;
        int jj = 0;
        double quarter;
        /*final ArrayAdapter<String> adapter1;

        final ArrayAdapter<String> adapter3;*/
//        ArrayList<String> tempL = new ArrayList<String>();

//        ArrayList<String> tempLb = new ArrayList<String>();

//        for (int i =1; i<=3; i++) {
//            lectures_id.clear();
//            lectures.clear();
//
//            if (i == 1) {
        jj = 0;
        lectures_id.clear();
        lectures.clear();
        lectures_id = workWithDB(4,student_id,1,null);
        j = lectures_id.size();
//        temp.clear();
        for (int k = 0; k<j;k++) {
            temp = workWithDB(5,student_id,0,lectures_id.get(k));
            //{"date,is_visit,grade,number,practice,theory,report"}
            if (Integer.parseInt(temp.get(1)) == 0) {
                lectures.add(temp.get(0));
            } else
                jj++;
        }
        quarter = j/4.00;
        if ((jj<quarter)||(jj==0)) {//условное форматирование
            textL.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (jj <=quarter*2) {
            textL.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (jj <=quarter*3) {
            textL.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            textL.setBackgroundColor(Color.parseColor("#c7f39f"));
        }
        int temp_element;
        /*temp_element = Integer.parseInt(lectures_att.get(Integer.parseInt(checked_item)));
        if ((temp_element<25)||(temp_element==0)) {//условное форматирование
            textL.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (temp_element <=50) {
            textL.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (temp_element <=75) {
            textL.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            textL.setBackgroundColor(Color.parseColor("#c7f39f"));
        }*/
//        textL.setText(jj+"/"+j);
        textL.setText(lectures_string.get(Integer.parseInt(checked_item)));
        ArrayList<String> tempL = new ArrayList<String>(lectures);

        ArrayList<String> values = new ArrayList<String>();
        int summ_grade =0;
        int average =0;
        double average_d =0;
        String temp_string = "";
        String temp_string1 = "";
        String temp_string2 = "";
        int temp_int=0;
        values.add("    дата        балл");
//        values.add("___________________");
        jj = 0;
        lectures_id.clear();
        lectures.clear();
//        quarter = 0;
        lectures_id = workWithDB(4,student_id,2,null);
        j = lectures_id.size();
//        temp.clear();
        for (int k = 0; k<j;k++) {
            temp = workWithDB(5,student_id,0,lectures_id.get(k));
            //{"date,is_visit,grade,number,practice,theory,report"}
            if (Integer.parseInt(temp.get(1)) == 0) {
                lectures.add(temp.get(0));
            } else {
                jj++;
            }

            temp_int = 6-(temp.get(0).length()-9)*2;
//            Log.d(LOG_TAG,"TEMP INT = "+temp_int);
            for (int i = 1; i<=temp_int;i++) {
                temp_string += " ";
            }
            values.add(temp.get(0)+temp_string+temp.get(2));
            temp_string = "";
            summ_grade += Integer.parseInt(temp.get(2));
            if (k==(j-1)) {
                average_d = summ_grade/(double)j;
                average = (int)Math.round(average_d);
            }

        }
        if (values.size()<2) {
            values.clear();//если нет занятий, убираем заголовок
        }
        quarter = j/4.00;
//        Log.d(LOG_TAG,"QUARTER = "+String.format("%.2f",quarter));
        if ((jj<quarter)||(jj==0)) {//условное форматирование
            textPracP.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (jj <=quarter*2) {
            textPracP.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (jj <=quarter*3) {
            textPracP.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            textPracP.setBackgroundColor(Color.parseColor("#c7f39f"));
        }
        textPracP.setText(jj+"/"+j);
        if (average<=24) {//условное форматирование
            textPracU.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (average <=50) {
            textPracU.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (average <=75) {
            textPracU.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            textPracU.setBackgroundColor(Color.parseColor("#c7f39f"));
        }
        textPracU.setText(Integer.toString(average));
        ArrayList<String> tempP = new ArrayList<String>(lectures);
        ArrayList<String> tempPV = new ArrayList<String>(values);

        jj = 0;
        lectures_id.clear();
        lectures.clear();
        values.clear();
        summ_grade = 0;
        average = 0;
//        quarter = 0;
        temp_string = "";
        values.add("№    дата      балл   т   п   о");
//        values.add("__________________________");
        lectures_id = workWithDB(4,student_id,3,null);
        j = lectures_id.size();
//        temp.clear();
        for (int k = 0; k<j;k++) {
            temp = workWithDB(5,student_id,0,lectures_id.get(k));
            //{"date,is_visit,grade,number,practice,theory,report"}
            if (Integer.parseInt(temp.get(1)) == 0) {
                lectures.add(temp.get(0));
            } else {
                jj++;
            }

            temp_int = 5-(temp.get(0).length()-9)*2;
//            Log.d(LOG_TAG,"TEMP INT = "+temp_int);
            for (int i = 1; i<=temp_int;i++) {
                temp_string1 += " ";
            }
            temp_int = 4-(temp.get(2).length()-2)*2;
//            Log.d(LOG_TAG,"TEMP INT = "+temp_int);
            for (int i = 1; i<=temp_int;i++) {
                temp_string2 += " ";
            }

            temp_string = temp.get(3)+"  "+temp.get(0)+temp_string1+temp.get(2)+temp_string2;
            if (temp.get(5).equals("1")) {
                temp_string += "●  ";
            } else temp_string += "○  ";

            if (temp.get(4).equals("1")) {
                temp_string += "●  ";
            } else temp_string += "○  ";

            if (temp.get(6).equals("1")) {
                temp_string += "●";
            } else temp_string += "○";

            values.add(temp_string);
            temp_string1 = "";
            temp_string2 = "";

//            values.add(temp.get(3)+"  "+temp.get(0)+"  "+temp.get(2)+" "+temp.get(4)+"  "+temp.get(5)+"  "+temp.get(6));
            summ_grade += Integer.parseInt(temp.get(2));
            if (k==(j-1)) {
                average_d = summ_grade/(double)j;
                average = (int)Math.round(average_d);
            }
        }
        if (values.size()<2) {
            values.clear();//если нет занятий, убираем заголовок
        }
        quarter = j/4.00;
        /*Log.d(LOG_TAG,"QUARTER *2 = "+String.format("%.2f",quarter*2.00));
        Log.d(LOG_TAG,"QUARTER *3 = "+String.format("%.2f",quarter*3.00));*/
        if ((jj<quarter)||(jj==0)) {//условное форматирование
            textLabP.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (jj <=quarter*2.00) {
            textLabP.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (jj <=quarter*3.00) {
            textLabP.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            textLabP.setBackgroundColor(Color.parseColor("#c7f39f"));
        }
        textLabP.setText(jj+"/"+j);
        if (average<=24) {
            textLabU.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (average <=50) {
            textLabU.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (average <=75) {
            textLabU.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            textLabU.setBackgroundColor(Color.parseColor("#c7f39f"));
        }
        textLabU.setText(Integer.toString(average));
        ArrayList<String> tempLb = new ArrayList<String>(lectures);
        ArrayList<String> tempLbV = new ArrayList<String>(values);



        /*            textL.setText(jj+"/"+j);
                    j=0;
                    jj=0;
                    tempL = lectures;
                    Log.d(LOG_TAG,"lectures on i="+i+tempL.toString());
//            }
            if (i == 2) {
                    textPracP.setText(jj+"/"+j);
                    tempP = new ArrayList<String>(lectures);
                    Log.d(LOG_TAG,"lectures on i="+i+tempP.toString());
                    j=0;
                    jj=0;
            }
            if (i == 3) {
                    textLabP.setText(jj+"/"+j);
                    tempLb = lectures;
                    Log.d(LOG_TAG,"lectures on i="+i+tempLb.toString());
                    j=0;
                    jj=0;
            }

        }*/

//        Log.d(LOG_TAG,"TEMP P ="+tempP.toString());

        /*adapter1 = new ArrayAdapter<String>(this,
                R.layout.list_item_popup, tempL);*/
        final ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,R.layout.list_item_popup, tempL);
        final ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this,R.layout.list_item_popup, tempP);
        final ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this,R.layout.list_item_popup, tempLb);
        final ArrayAdapter<String> adapter4 = new ArrayAdapter<String>(this,R.layout.list_item_popup, tempPV);
        final ArrayAdapter<String> adapter5 = new ArrayAdapter<String>(this,R.layout.list_item_popup, tempLbV);
        /*adapter3 = new ArrayAdapter<String>(this,
                R.layout.list_item_popup, tempLb);*/



        LayoutInflater layoutInflater
                = (LayoutInflater)getBaseContext()
                .getSystemService(LAYOUT_INFLATER_SERVICE);
        final View popupView = layoutInflater.inflate(R.layout.popup_layout, null);

//        popupWindow.setAnimationStyle(0);

        final ListView popup_list = (ListView) popupView.findViewById(R.id.listViewPop);


        /*popup_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (popupWindow.isShowing()) {
                    popupWindow.dismiss();
                }
            }
        });*/
        final PopupWindow popupWindow = new PopupWindow(
                popupView,
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT);
                /*150,
                LayoutParams.WRAP_CONTENT);*/


        int screen_width = getWindowManager().getDefaultDisplay().getWidth();

        final int labs_width = (screen_width/2)+(screen_width/8);
        final int prac_width = (screen_width/2)-(screen_width/12);
        final int pos_width = (screen_width/3);
//        TextView kvadrat = (TextView) builderView.findViewById(R.id.textViewL);
        final int kv_size = screen_width/7;
        Log.d(LOG_TAG,"KVADRAT = "+kv_size);



        final LayoutParams p2 = popup_list.getLayoutParams();
        popupView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(LOG_TAG,"CLICK2");
                popupWindow.dismiss();
                click1=false;
                click2=false;
                click3=false;
                click4=false;
                click5=false;
            }
        });
        popup_list.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(LOG_TAG,"CLICK");
                popupWindow.dismiss();
                click1=false;
                click2=false;
                click3=false;
                click4=false;
                click5=false;
            }
        });

        /*popup_list.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(LOG_TAG,"SELECT");
                popupWindow.dismiss();
                click1=false;
                click2=false;
                click3=false;
                click4=false;
                click5=false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        textL.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                p2.width = pos_width;//70
                popup_list.setLayoutParams(p2);
                popup_list.setAdapter(adapter1);

//                if (!click) {
                //если открыто окно и 2 или 3 клик = тру, тогда нужно закрыть и открыть.
                //если стоит только 1 клик, то просто закрыть.
                if (click1&&!click2&&!click3&&!click4&&!click5){
                    if (popupWindow.isShowing()) {
//                        Log.d(LOG_TAG,"Close lect");
    //                    click = true;
                        popupWindow.dismiss();
                        click1 = false;
                    }
                } else if (!click1&&!click2&&!click3&&!click4&&!click5){
//                    Log.d(LOG_TAG,"Open lect");
//                    click = false;
                    popupWindow.showAsDropDown(textL, kv_size, -(kv_size));
                    click1 = true;
                } else if(!click1&&(click2||click3||click4||click5)) {
//                    Log.d(LOG_TAG,"Close old");
                    popupWindow.dismiss();
//                    Log.d(LOG_TAG, "Open lect");
                    popupWindow.showAsDropDown(textL, kv_size, -(kv_size));
                    click1 = true;
                    click2=false;
                    click3=false;
                    click4=false;
                    click5=false;
                }


            }
        });

        textPracP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                p2.width = pos_width;//70
                popup_list.setLayoutParams(p2);
                popup_list.setAdapter(adapter2);
                if (click2&&!click1&&!click3&&!click4&&!click5){
                    if (popupWindow.isShowing()) {
//                        Log.d(LOG_TAG,"Close pracP");
                        //                    click = true;
                        popupWindow.dismiss();
                        click2 = false;
                    }
                } else if (!click2&&!click1&&!click3&&!click4&&!click5){
//                    Log.d(LOG_TAG,"Open pracP");
//                    click = false;
                    popupWindow.showAsDropDown(textPracP, kv_size, -(kv_size));
                    click2 = true;
                } else if(!click2&&(click1||click3||click4||click5)) {
//                    Log.d(LOG_TAG,"Close old");
                    popupWindow.dismiss();
//                    Log.d(LOG_TAG,"Open pracP");
                    popupWindow.showAsDropDown(textPracP, kv_size, -(kv_size));
                    click2 = true;
                    click1=false;
                    click3=false;
                    click4=false;
                    click5=false;
                }

//                if (!click) {
                /*if (popupWindow.isShowing()) {
                    Log.d(LOG_TAG,"Close lect2");
//                    click = true;
                    popupWindow.dismiss();
                } else {
                    Log.d(LOG_TAG,"Open lect2");
//                    click = false;
                    popupWindow.showAsDropDown(textL, 40, -40);
                }*/


            }
        });

        textLabP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                p2.width = pos_width;//
                popup_list.setLayoutParams(p2);
                popup_list.setAdapter(adapter3);
                if (click3&&!click2&&!click1&&!click4&&!click5){
                    if (popupWindow.isShowing()) {
//                        Log.d(LOG_TAG,"Close labP");
                        //                    click = true;
                        popupWindow.dismiss();
                        click3 = false;
                    }
                } else if (!click3&&!click2&&!click1&&!click4&&!click5){
//                    Log.d(LOG_TAG,"Open labP");
//                    click = false;
                    popupWindow.showAsDropDown(textLabP, kv_size, -kv_size);
                    click3 = true;
                } else if(!click3&&(click2||click1||click4||click5)) {
//                    Log.d(LOG_TAG,"Close old");
                    popupWindow.dismiss();
//                    Log.d(LOG_TAG,"Open labP");
                    popupWindow.showAsDropDown(textLabP, kv_size, -kv_size);
                    click3 = true;
                    click2=false;
                    click1=false;
                    click4=false;
                    click5=false;
                }
//                if (!click) {
                /*if (popupWindow.isShowing()) {
                    Log.d(LOG_TAG,"Close lect3");
//                    click = true;
                    popupWindow.dismiss();
                } else {
                    Log.d(LOG_TAG,"Open lect3");
//                    click = false;
                    popupWindow.showAsDropDown(textL, 40, -40);
                }*/


            }
        });
//        final RelativeLayout.LayoutParams p2 = new RelativeLayout.LayoutParams(200,ViewGroup.LayoutParams.WRAP_CONTENT);


        textPracU.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                popup_list.setAdapter(adapter4);
                p2.width = prac_width;//100
                popup_list.setLayoutParams(p2);
                popupView.setLayoutParams(p2);
//                popupWindow.setWidth(100);
//                popupView.setCon


                if (click4&&!click2&&!click3&&!click1&&!click5){
                    if (popupWindow.isShowing()) {
//                        Log.d(LOG_TAG,"Close pracU");
                        //                    click = true;
                        popupWindow.dismiss();
                        click4 = false;
                    }
                } else if (!click4&&!click2&&!click3&&!click1&&!click5){
//                    Log.d(LOG_TAG,"Open pracU");
//                    click = false;
                    /*p2.width = 200;
                    popup_list.setLayoutParams(p2);
                    popupView.setLayoutParams(p2);*/
                    popupWindow.showAsDropDown(textPracU, -prac_width, -kv_size);
                    int p_h = popup_list.getWidth();
                    Log.d(LOG_TAG,"LIST WIDTH = "+p_h);
//                    popupWindow.update(100,p_h);
                    Log.d(LOG_TAG,"WINDOW WIDTH = "+popupWindow.getWidth());
                    click4 = true;
                } else if(!click4&&(click2||click3||click1||click5)) {
//                    Log.d(LOG_TAG,"Close old");
                    popupWindow.dismiss();
//                    Log.d(LOG_TAG,"Open pracU");
                    /*p2.width = 100;
                    popup_list.setLayoutParams(p2);
                    popupView.setLayoutParams(p2);*/
                    popupWindow.showAsDropDown(textPracU, -prac_width, -kv_size);
                    int p_h = popup_list.getWidth();
                    Log.d(LOG_TAG,"LIST WIDTH = "+p_h);
//                    popupWindow.update(100,p_h);
                    Log.d(LOG_TAG,"WINDOW WIDTH = "+popupWindow.getWidth());
                    click4 = true;
                    click2=false;
                    click3=false;
                    click1=false;
                    click5=false;
                }
            }
        }); 
        textLabU.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                p2.width = labs_width;//150
                popup_list.setLayoutParams(p2);
                popup_list.setAdapter(adapter5);
                if (click5&&!click2&&!click3&&!click4&&!click1){
                    if (popupWindow.isShowing()) {
//                        Log.d(LOG_TAG,"Close labU");
                        //                    click = true;
                        popupWindow.dismiss();
                        click5 = false;
                    }
                } else if (!click5&&!click2&&!click3&&!click4&&!click1){
//                    Log.d(LOG_TAG,"Open labU");
//                    click = false;
                    popupWindow.showAsDropDown(textLabU, -labs_width, -kv_size);
                    click5 = true;
                } else if(!click5&&(click2||click3||click4||click1)) {
//                    Log.d(LOG_TAG,"Close old");
                    popupWindow.dismiss();
//                    Log.d(LOG_TAG,"Open labU");
                    popupWindow.showAsDropDown(textLabU, -labs_width, -kv_size);
                    click5 = true;
                    click2=false;
                    click3=false;
                    click4=false;
                    click1=false;
                }
            }
        });
       /* textPracP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*LayoutInflater layoutInflater
                        = (LayoutInflater)getBaseContext()
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.popup_layout, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
//        popupWindow.setAnimationStyle(0);

                final ListView popup_list = (ListView) popupView.findViewById(R.id.listViewPop);

                popup_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if (popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    }
                });
                popup_list.setAdapter(adapter2);*//*
//                if (!click) {

                *//*ListView popup_list = (ListView) popupView.findViewById(R.id.listViewPop);
                popup_list.setAdapter(adapter2);
                popup_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        popupWindow.dismiss();
                    }
                });
*//*
                if (popupWindow.isShowing()) {
                    Log.d(LOG_TAG,"Close prac");
//                    click = true;
                    popupWindow.dismiss();
                } else {
                    Log.d(LOG_TAG,"Open prac");
//                    click = false;
                    popupWindow.showAsDropDown(textPracP, 40, -40);
                }


            }
        });

        textLabP.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                *//*LayoutInflater layoutInflater
                        = (LayoutInflater)getBaseContext()
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                final View popupView = layoutInflater.inflate(R.layout.popup_layout, null);
                final PopupWindow popupWindow = new PopupWindow(
                        popupView,
                        LayoutParams.WRAP_CONTENT,
                        LayoutParams.WRAP_CONTENT);
//        popupWindow.setAnimationStyle(0);

                final ListView popup_list = (ListView) popupView.findViewById(R.id.listViewPop);

                popup_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        if (popupWindow.isShowing()) {
                            popupWindow.dismiss();
                        }
                    }
                });
                popup_list.setAdapter(adapter3);*//*
//                if (!click) {

               *//* ListView popup_list = (ListView) popupView.findViewById(R.id.listViewPop);
                popup_list.setAdapter(adapter3);
                popup_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        popupWindow.dismiss();
                    }
                });*//*

                if (popupWindow.isShowing()) {
                    Log.d(LOG_TAG,"Close lab");
//                    click = true;
                    popupWindow.dismiss();
                } else {
                    Log.d(LOG_TAG,"Open lab");
//                    click = false;
                    popupWindow.showAsDropDown(textLabP, 40, -40);
                }


            }
        });*/





        alert.setView(builderView);

        //tvDisplayDate = (TextView) findViewById(R.id.tvDate);





//        final CheckBox checkBox1 = new CheckBox(this);
//        checkBox1.setText("Присутствовал(а)");
//        alert.setView(checkBox1);
//        final EditText input2 = new EditText(this);
//        input2.setHint(checked_item);
//        alert.setView(input2);

        /*alert.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {*/
                /*int is_checked=0;
                if (chb.isChecked()) {
                    is_checked = 1;
                }
                //writeToDB(1,0,0,2,2,2,edtDate.getText().toString(),is_checked,Integer.parseInt(student_id),edt.getText().toString(),null);
                Toast toast = Toast.makeText(getApplicationContext(), "Сохранено", Toast.LENGTH_SHORT);
                toast.show();*/
                /*if (chb.isChecked()) {
                    Toast toast = Toast.makeText(getApplicationContext(), "Присутствовал(а)", Toast.LENGTH_SHORT);
                    toast.show();
                } else {
                    Toast toast = Toast.makeText(getApplicationContext(), "Отсутствовал(а)", Toast.LENGTH_SHORT);
                    toast.show();
                }*/
                /*String value = "";
                value = input2.getText().toString();
//                if (value.isEmpty() == false) {
                    Toast toast = Toast.makeText(getApplicationContext(), value, Toast.LENGTH_SHORT);
                    toast.show();*/
//                } else {
//                    Toast toast = Toast.makeText(getApplicationContext(), "Не сохранено, пустое значение.", Toast.LENGTH_LONG);
//                    toast.show();
//                }

                // Получили значение введенных данных!
           /* }
        });*/

        alert.setNegativeButton("Закрыть", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                // Если отменили.
                if (popupWindow.isShowing()) {
//                    Log.d(LOG_TAG,"Close labP");
                    //                    click = true;
                    popupWindow.dismiss();
                    click1 = false;
                    click2 = false;
                    click3 = false;
                    click4 = false;
                    click5 = false;
                }
            }
        });

        AlertDialog alertDialog = alert.create();
//        alertDialog.getWindow().setLayout(600, 600);
        alertDialog.show();



//        alert.show();
//    }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }



    public SQLiteDatabase initDB (DBHelper dbh) {
        Log.d(LOG_TAG, "--- GET DB---");
        SQLiteDatabase tdb = dbh.getWritableDatabase();
        return (tdb);
    }

    public void writeToDB (int type, int type_l,int number, int prac, int teor, int otch, String date,int visit, int grade, int id_student, String note, String id_field) {
        Log.d(LOG_TAG, "--- writeToDB ---");
        ContentValues cv = new ContentValues();
        /*DBHelper dbh = new DBHelper(this);
        SQLiteDatabase db = initDB(dbh);*/
//        int last_id;
//        dbh.onOpen(db);
        String st_id = String.valueOf(id_student);
        ArrayList<String> loaded = new ArrayList<String>();
        switch (type) {
            case 0://успеваемость
//                if (loaded.isEmpty()==false) {
                   // last_id = Integer.getInteger(loaded.get(loaded.size()-1)); //взять последний id из таблицы

//                    String temp = loaded.get(loaded.size()-1);
//                    last_id = Integer.parseInt(temp);

                /*} else {
                    last_id = 0;
                }
                last_id++;*/
//                Log.d(LOG_TAG, "--- LAST_ID = " + last_id);
//                Log.d(LOG_TAG, "--- STUDENT_ID = " + id_student);
                cv.clear();
                //cv.put("id",last_id);
                cv.put("type",type_l);
                if (type_l == 3) {
                    cv.put("number",number);
                    cv.put("practice",prac);
                    cv.put("theory",teor);
                    cv.put("report",otch);
                }
                cv.put("date",date);
                cv.put("is_visit",visit);
                cv.put("grade",grade);

                cv.put("note",note);
                cv.put("id_student",id_student);
                db.insert("lessons", null, cv);
//                loaded = workWithDB(3,st_id);
//                Log.d(LOG_TAG, "--- LOADED = " + loaded.toString());
            break;
            case 1://посещаемость
                cv.clear();
                cv.put("date",date);
                cv.put("is_visit",visit);
                cv.put("note",note);
                cv.put("id_student",id_student);
                db.insert("attendance", null, cv);
//                loaded = workWithDB(4,st_id);
//                Log.d(LOG_TAG, "--- LOADED = " + loaded.toString());
            break;
            case 2://обновление записи в посещаемости
                Log.d(LOG_TAG, "--- UPDATE--- ");
                cv.clear();
                cv.put("type",type_l);
                if (type_l == 3) {
                    cv.put("number",number);
                    cv.put("practice",prac);
                    cv.put("theory",teor);
                    cv.put("report",otch);
                }
                cv.put("date",date);
                cv.put("is_visit",visit);
                cv.put("grade",grade);

                cv.put("note",note);
                cv.put("id_student",id_student);
                db.update("lessons",cv,"id="+id_field,null);
//                db.insert("lessons", null, cv);
            break;
        }
        rating();
    }

//    public ArrayList<String> workWithDB (int type, String id,SQLiteDatabase db,DBHelper dbh) {
    public static ArrayList<String> workWithDB (int type, String id, int type_l, String number) {

//        dbh.onOpen(db);
        ArrayList<String> data = new ArrayList<String>();

        // Подключаемся к БД

        /*DBHelper dbh = new DBHelper(this);
        Log.d(LOG_TAG, "--- CREATE---");
        SQLiteDatabase db = dbh.getWritableDatabase();*/

       /* Log.d(LOG_TAG, "--- Update mytabe: ---");
        ContentValues cv = new ContentValues();*/
        // подготовим значения для обновления
        //cv.put("id_group", 2);
        /*// обновляем по id
        db.update("students", cv, "id = ?",
                new String[] { "2" });*/
//        Log.d(LOG_TAG, "updated rows count = " + updCount);

        // Описание курсора
        Cursor c;

        /*// выводим результат объединения
        // используем rawQuery
        Log.d(LOG_TAG, "--- INNER JOIN with rawQuery---");
        String sqlQuery = "select ST.name as Name, GR.name as Group "
                + "from students as ST "
                + "inner join groups as GR "
                + "on ST.id_group = GR.id ";
//                + "where salary > ?";
        c = db.rawQuery(sqlQuery,null);
        logCursor(c);
        c.close();
        Log.d(LOG_TAG, "--- ---");*/
        String table = "";
//        String columns[] = { "ST.name as Name", "GR.name as Group" };
        String[] columns = null;
        String selection = null;
        String[] selectionArgs = null;
        String orderBy = null;
        String type_less = Integer.toString(type_l);
        String number_less = number;
        Log.d(LOG_TAG,"Type of lesson = "+type_less+", number = "+number_less);
        // выводим результат объединения
        // используем query
        switch (type) {
            case 1:
                Log.d(LOG_TAG, "--- STUDENTS---");
//        String table = "students as ST inner join groups as GR on ST.id_group = GR.id";
                table = "students";
//        String columns[] = { "ST.name as Name", "GR.name as Group" };
                columns = new String[] {"id,name"};
                selection = "id_group = ?";
                selectionArgs = new String[] {id};

            break;
            case 2:
                Log.d(LOG_TAG, "--- GROUPS---");
//        String table = "students as ST inner join groups as GR on ST.id_group = GR.id";
                table = "groups";
//        String columns[] = { "ST.name as Name", "GR.name as Group" };
                columns = new String[] {"id,name"};
            break;
            case 3://запрос на успеваемость
                Log.d(LOG_TAG, "--- LESSONS---");
//        String table = "students as ST inner join groups as GR on ST.id_group = GR.id";
                table = "lessons";
//        String columns[] = { "ST.name as Name", "GR.name as Group" };
                columns = new String[] {"id,date,type,number,practice,theory,report,grade,note,is_visit"};//+выбрать id записи (если она не пустая, потом ее update)
                if (type_l == 3) {
                    selection = "id_student = ? AND type = ? AND number = ?";
//                selection = "id_student =?";
                    selectionArgs = new String[] {id,type_less,number_less};
                }
                if ((type_l == 1) || (type_l == 2)) {
                    selection = "id_student = ? AND type = ? AND date = ?";
//                selection = "id_student =?";
                    selectionArgs = new String[] {id,type_less,number_less};
                }

//                selectionArgs = new String[] {id};
                //orderBy = "id DESC LIMIT 1";
            break;

            case 4://запрос на список id занятий студента
                Log.d(LOG_TAG, "--- this type ID---");
//        String table = "students as ST inner join groups as GR on ST.id_group = GR.id";
                table = "lessons";
//        String columns[] = { "ST.name as Name", "GR.name as Group" };
                columns = new String[] {"id"};
                    selection = "id_student = ? AND type = ?";
                    selectionArgs = new String[] {id,type_less};



                //orderBy = "id DESC LIMIT 1";
                break;
            case 5://запрос на список занятие студента
                Log.d(LOG_TAG, "--- this lesson---");
//        String table = "students as ST inner join groups as GR on ST.id_group = GR.id";
                table = "lessons";
//        String columns[] = { "ST.name as Name", "GR.name as Group" };
                columns = new String[] {"date,is_visit,grade,number,practice,theory,report"};
                selection = "id_student = ? AND id = ?";
                selectionArgs = new String[] {id,number_less};



                //orderBy = "id DESC LIMIT 1";
                break;

            case 6://запрос на успеваемость
                Log.d(LOG_TAG, "--- LESSONS XML---");
//        String table = "students as ST inner join groups as GR on ST.id_group = GR.id";
                table = "lessons";
//        String columns[] = { "ST.name as Name", "GR.name as Group" };
                columns = new String[] {"id,date,type,number,practice,theory,report,grade,note,is_visit"};
                selection = "id_student = ?";
//                selection = "id_student =?";
                selectionArgs = new String[] {id};
                break;
        }

        c = db.query(table, columns, selection, selectionArgs, null, null, null);
        data = logCursor(c, data);
        c.close();
//        Log.d(LOG_TAG, "--- ---");
        /*for (int i =0; i<data.size();i++) {

        }*/
        Log.d(LOG_TAG,"RESULT = "+ data.toString());


        // закрываем БД
//        dbh.close();


        return data;
    }
    static ArrayList<String> logCursor(Cursor c, ArrayList<String> data) {
        if (c != null) {
            if (c.moveToFirst()) {
                String str;
                do {
                    str = "";
                    for (String cn : c.getColumnNames()) {
//                        str = str.concat(cn + " = " + c.getString(c.getColumnIndex(cn)) + "; ");
                        data.add(c.getString(c.getColumnIndex(cn)));
//                        data.add(str);
                    }
                    Log.d(LOG_TAG, str);
                } while (c.moveToNext());
            }
        } else
            Log.d(LOG_TAG, "Cursor is null");
        return (data);
    }
}
