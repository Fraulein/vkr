package com.example.myapplication5.app;


import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class CustomList extends ArrayAdapter<String> {
    private  Activity context;
    private ArrayList<String> studentName;
    private  ArrayList<String> studentRate;
    private  ArrayList<String> studentAtt;
    final String LOG_TAG = "myLogsG2";

    public CustomList(Activity context, ArrayList<String> studentName, ArrayList<String> studentRate, ArrayList<String> studentAtt) {
        super(context, R.layout.list_item_stata,studentName);
//        Log.d(LOG_TAG,"CUSTOM LIST");
        this.context = context;
        this.studentName = studentName;
        this.studentRate = studentRate;
        this.studentAtt = studentAtt;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
//        Log.d(LOG_TAG,"GET VIEW");
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.list_item_stata, null, true);
        TextView txtStudent = (TextView) rowView.findViewById(R.id.studentName);
        TextView txtRate = (TextView) rowView.findViewById(R.id.studentRate);
        TextView txtAtt = (TextView) rowView.findViewById(R.id.studentAtt);
        int temp_rate = Integer.parseInt(studentRate.get(position));
        int temp_att = Integer.parseInt(studentAtt.get(position));
        if (temp_rate<25) {
            txtRate.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (temp_rate<=50) {
            txtRate.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (temp_rate<=75) {
            txtRate.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            txtRate.setBackgroundColor(Color.parseColor("#c7f39f"));
        }

        if (temp_att<25) {
            txtAtt.setBackgroundColor(Color.parseColor("#ffffff"));
        } else if (temp_att<=50) {
            txtAtt.setBackgroundColor(Color.parseColor("#ffc3c1"));
        } else if (temp_att<=75) {
            txtAtt.setBackgroundColor(Color.parseColor("#fff1c2"));
        } else {
            txtAtt.setBackgroundColor(Color.parseColor("#c7f39f"));
        }
        txtStudent.setText(studentName.get(position));
//        Log.d(LOG_TAG,studentName.get(position));
        txtRate.setText(studentRate.get(position));
        txtAtt.setText(studentAtt.get(position));
        return rowView;
    }
}
