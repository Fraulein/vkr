package com.example.myapplication5.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;


public class Group_stat extends Activity{
    final String LOG_TAG = "myLogsG";
    TableLayout table;
    static Activity activity;
    ListView list;
    ArrayList<String> studentRate = new ArrayList<String>();
    ArrayList<String> studentAtt = new ArrayList<String>();
    double koef_prac = 0.5;
    double koef_lab = 0.5;
    static String gr_name;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_stat);
        activity = this;
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();
        gr_name = "";
        double average_d;
        int average,pr_r,lab_r,l_a,pr_a,lab_a;
        if (extras != null) {
            gr_name = extras.getString("group_name");
        }

        this.setTitle("Статистика гр. "+gr_name);

        for (int i=0; i< MainActivity.students.size(); i++) {
//            if (!MainActivity.practice_rating.get(i).equals("")) {
                pr_r = Integer.parseInt(MainActivity.practice_rating.get(i));
//            } else {
//                pr_r = 0;
//            }

//            if (!MainActivity.laboratory_rating.get(i).equals("")) {
                lab_r = Integer.parseInt(MainActivity.laboratory_rating.get(i));
//            } else {
//                lab_r = 0;
//            }
            average_d = pr_r*koef_prac+lab_r*koef_lab;
            average = (int)Math.round(average_d);
            studentRate.add(Integer.toString(average));

//            if (!MainActivity.lectures_att.get(i).equals("")) {
                l_a = Integer.parseInt(MainActivity.lectures_att.get(i));
//            } else {
//                l_a = 0;
//            }

//            if (!MainActivity.practice_att.get(i).equals("")) {
                pr_a = Integer.parseInt(MainActivity.practice_att.get(i));
//            } else {
//                pr_a = 0;
//            }

//            if (!MainActivity.laboratory_att.get(i).equals("")) {
                lab_a = Integer.parseInt(MainActivity.laboratory_att.get(i));
//            } else {
//                lab_a = 0;
//            }

            average_d = (l_a+pr_a+lab_a)/3.0;
            average = (int)Math.round(average_d);
            studentAtt.add(Integer.toString(average));
        }


        CustomList adapter = new CustomList(activity,MainActivity.students,studentRate,studentAtt);
        list = (ListView) findViewById(R.id.listViewGroupStat);
        list.setAdapter(adapter);
//        ArrayList<String> s = new ArrayList<String>();
//        ;

        /*ArrayList<String> data3 = MainActivity.workWithDB(1,gr_id,0,null);
        st_id.clear();
        students.clear();


        for (int i = 1; i< data3.size()+1; i++) {
            if ((i%2)!=0) {
//                st_id.add(data3.get(i-1));
            } else {
                students.add(data3.get(i-1));
            }
        }*/
        //Log.d(LOG_TAG,"DATA"+data2);
        /*ListView lvActionMode;
        lvActionMode = (ListView) findViewById(R.id.listViewGroupStat);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, MainActivity.students);
        lvActionMode.setAdapter(adapter);*/
//        createTable();
    }

   /* public void createTable(ArrayList<String> studentRate,ArrayList<String> studentAtt) {
        *//*TableRow Row = new TableRow(this);
        ArrayList<TableRow> rows = new ArrayList<TableRow>();
        TextView studentName = new TextView(this);
        for (int i = 0; i<MainActivity.students.size(); i++) {
            Row.removeView(studentName);
            studentName.setText(MainActivity.students.get(i));
            Row.addView(studentName);
            rows.add(Row);
            table.addView(rows.get(i));
        }*//*

    }*/

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.group_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected (MenuItem item) {
        String llog;
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
//                Log.d(LOG_TAG,"Back");
//                this.finish();
//                NavUtils.navigateUpFromSameTask(this);
                return true;
            case R.id.toXml:
                llog = DBHelper.exportToXML(gr_name);
                try {
                    /*OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput("xml/group.xml", Context.MODE_PRIVATE));
                    outputStreamWriter.write(llog);
                    outputStreamWriter.close();*/
                    FileOutputStream fos = openFileOutput("group.xml", Context.MODE_PRIVATE);
                    fos.write(llog.getBytes());
                    fos.close();
                    /*java.io.FileWriter fileWriter = new FileWriter(getExternalFilesDir("group.xml"));
                    fileWriter.write(llog);
                    fileWriter.close();*/
                }
                catch (IOException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                }

//                PrintWriter out = new PrintWriter("xml/group.xml");
                Log.d(LOG_TAG,"XML = "+llog);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}
