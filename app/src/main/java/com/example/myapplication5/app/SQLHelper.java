package com.example.myapplication5.app;

//package com.example.myapplication2.app;

        import android.content.Context;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;
        import android.util.Log;

public class SQLHelper extends SQLiteOpenHelper  {

    private static final String DATABASE_NAME = "demo.db";
    public static final  String KEY_NAME = "name";
    public static final  String KEY_SURNAME = "surname";
    public static final  String KEY_ADDS = "comm";
    private static final String DATABASE_TABLE = "main";
    private static final int DATABASE_VERSION = 4;
    final String DATABASE_CREATE = "CREATE TABLE "+DATABASE_TABLE+" (_id integer primary key autoincrement, " + KEY_NAME + " text not null, " + KEY_SURNAME + " text not null, " + KEY_ADDS + " text not null);";
    //final String DATABASE_CREATE = "CREATE TABLE "+DATABASE_TABLE+" (_id integer primary key autoincrement, name text)";
    final String DROP_TABLE = "DROP TABLE IF EXISTS "+DATABASE_TABLE;

    SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.v("SQLHelper", "Сюда дошли!");
    }

    @Override
    public void onCreate (SQLiteDatabase sqLiteDatabase) {
        Log.v("SQLHelper","Создали новую базу");
        sqLiteDatabase.execSQL(DATABASE_CREATE);
    }

    @Override
    public void onUpgrade (SQLiteDatabase sqLiteDatabase, int i, int i2) {

        Log.v("SQLHelper", "Обновили базу " + i + " to " + i2 + ", все данные были потерты");
        sqLiteDatabase.execSQL(DROP_TABLE);
        onCreate(sqLiteDatabase);
    }


}

