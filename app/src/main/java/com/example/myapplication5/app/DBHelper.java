package com.example.myapplication5.app;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import org.xmlpull.v1.XmlSerializer;

import android.content.res.XmlResourceParser;
import android.app.Activity;
import android.util.Xml;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.util.ArrayList;

import static android.util.Xml.*;


class DBHelper extends SQLiteOpenHelper {
    final static String LOG_TAG = "myLogs3";
    private static final String DATABASE_TABLE = "lessons";
    final String DROP_TABLE = "DROP TABLE IF EXISTS "+DATABASE_TABLE;
    final Context contextM;

    public DBHelper(Context context) {
        // конструктор суперкласса
        super(context, "myDB", null, 1);
        contextM = context;
//            context.deleteDatabase("myDB");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        Log.d(LOG_TAG, "--- onCreate database ---");
        ContentValues cv = new ContentValues();
        // создаем таблицу с полями
        db.execSQL("create table students ("
                + "id integer primary key autoincrement,"
                + "name text,"
                + "id_group integer" + ");");



        //ArrayList<String> student_list = getDataFromXML("students",null);

           /* for (int i = 0; i<students_id.length; i++) {
                cv.clear();
                cv.put("id",students_id[i]);
                cv.put("name",cr_students[i]);
                if ((i+1)%2==0) {
                    cv.put("id_group",groups_id[0]);
                } else {
                    cv.put("id_group",groups_id[1]);
                }

                db.insert("students", null, cv);
            }*/

        db.execSQL("create table groups ("
                + "id integer primary key autoincrement,"
                + "name text"
                + ");");
        db.execSQL("create table lessons ("
                + "id integer primary key autoincrement,"
                + "type integer,"
                + "number integer,"
                + "date text,"
                + "is_visit integer,"
                + "grade integer,"
                + "practice integer,"
                + "theory integer,"
                + "report integer,"
                + "note text,"
                + "id_student integer" + ");");

        db.execSQL("create table attendance ("
                + "id integer primary key autoincrement,"
                + "date text,"
                + "is_visit integer,"
                + "note text,"
                + "id_student integer" + ");");

        ArrayList<String> group_list = getDataFromXML("group",null);
        Log.d(LOG_TAG,"GROUP_LIST"+ group_list.toString());
        ArrayList<Integer> group_list_id = new ArrayList<Integer>();
        int id_inc = 1;
        for (int i = 0; i<group_list.size();i++) {
            cv.clear();
//                cv.put("id",id_inc);
            cv.put("name",group_list.get(i));
            db.insert("groups",null,cv);
            group_list_id.add(id_inc);
            id_inc++;
        }



        ArrayList<String> student_list = new ArrayList<String>();
        id_inc = 1;
        for (int i = 0; i<group_list.size();i++) {
            student_list = getDataFromXML("students",group_list.get(i));
            Log.d(LOG_TAG,"group(i)"+ group_list.get(i));
            Log.d(LOG_TAG, "student_list"+student_list.toString());
            for (int j = 0; j<student_list.size();j++) {
                cv.clear();
//                    cv.put("id",id_inc);
                cv.put("name",student_list.get(j));
                cv.put("id_group",group_list_id.get(i));
                db.insert("students", null, cv);
                id_inc++;
            }
        }
            /*for (int i = 0; i<groups_id.length; i++) {
                cv.clear();
                cv.put("id",groups_id[i]);
                cv.put("name",cr_groups[i]);
                db.insert("groups",null,cv);
            }*/
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE);
        onCreate(db);
    }

    XmlPullParser prepareXpp() {
        return contextM.getResources().getXml(R.xml.data);
    }

    private ArrayList<String> getDataFromXML(String type, String group){
        //String tmp = "";
        ArrayList<String> data = new ArrayList<String>();
        String value = "";
//        ArrayList<String> students = new ArrayList<String>();
//        ArrayList<String> groups = new ArrayList<String>();
        Boolean flag = false;
        try {
            XmlPullParser xpp = prepareXpp();

            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    // начало документа
                    case XmlPullParser.START_DOCUMENT:
                        //Log.d(LOG_TAG, "START_DOCUMENT");
                        break;
                    // начало тэга
                    case XmlPullParser.START_TAG:
                        if (xpp.getName().equals("group")) {
                            value = xpp.getAttributeValue(0);
                            if (type.equals("group")) {
                                data.add(value);
                            }
                            Log.d(LOG_TAG, "VALUE "+value);
                        /*if ((type.equals("group")&&(xpp.getName().equals(type)))) {
                            data.add(xpp.getAttributeValue(0));
                            //Log.d(LOG_TAG, "GROUP");
                        } else*/ if ((type.equals("students"))&&(value.equals(group))) {
                                flag = true;
                            } else {
                                flag = false;
                            }

                        }


                        /*Log.d(LOG_TAG, "START_TAG: name = " + xpp.getName()
                                + ", depth = " + xpp.getDepth() + ", attrCount = "
                                + xpp.getAttributeCount());*/
                       /* tmp = "";
                        for (int i = 0; i < xpp.getAttributeCount(); i++) {
                            tmp = tmp + xpp.getAttributeName(i) + " = "
                                    + xpp.getAttributeValue(i) + ", ";
                        }*/
                        /*if (!TextUtils.isEmpty(tmp))
                            Log.d(LOG_TAG, "Attributes: " + tmp);*/
                        break;
                    // конец тэга
                    case XmlPullParser.END_TAG:
//                        Log.d(LOG_TAG, "END_TAG: name = " + xpp.getName());
                        break;
                    // содержимое тэга
                    case XmlPullParser.TEXT:
                        if (type.equals("students")&&(flag)) {
                            data.add(xpp.getText());
                            Log.d(LOG_TAG, "student = " + xpp.getText());
                        }

                        //Log.d(LOG_TAG, "text = " + xpp.getText());
                        break;

                    default:
                        break;
                }
                // следующий элемент
                xpp.next();
            }
//            Log.d(LOG_TAG, "END_DOCUMENT");

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;

    }

    static String exportToXML (String group) throws  IllegalArgumentException, IllegalStateException {
        /*File f = new File(getFilesDir(),"treasure.xml");
        FileOutputStream myFile=openFileOutput(f);
        Log.v("WriteFile","file created");*/
//        String group_name = MainActivity.groups.get(MainActivity.gr_id.indexOf(group));
        ArrayList<String> data = new ArrayList<String>();
        XmlSerializer xmlSerializer = newSerializer();
        StringWriter writer = new StringWriter();
        try {

        xmlSerializer.setOutput(writer);

        //Start Document

        xmlSerializer.startDocument("UTF-8", true);
        xmlSerializer.setFeature("http://xmlpull.org/v1/doc/features.html#indent-output", true);
        //Open Tag <file>
        xmlSerializer.startTag("", "file");

        XmlSerializer tempXml = Xml.newSerializer();
        StringWriter tempWriter =  new StringWriter();

        xmlSerializer.startTag("", "group");
        xmlSerializer.attribute("", "name", group);
        String group_id = MainActivity.gr_id.get(MainActivity.groups.indexOf(group));


        for (int i = 0; i < MainActivity.students.size(); i++) {
            Log.d(LOG_TAG,"st_id = "+MainActivity.st_id.get(i));
            data = MainActivity.workWithDB(6,MainActivity.st_id.get(i),0,null);
            xmlSerializer.startTag("", "student");
            xmlSerializer.attribute("", "name", MainActivity.students.get(i));
            int size = data.size();
            int j = 0,temp = 0;
            while (temp<=size-1) {
                xmlSerializer.startTag("","lesson");
                xmlSerializer.text(data.get(temp)+" "+data.get(temp+1)+" "+data.get(temp+2)+" "+data.get(temp+3)+" "+data.get(temp+4)+" "+data.get(temp+5)+" "+data.get(temp+6)+" "+data.get(temp+7)+" "+data.get(temp+8)+" "+data.get(temp+9));
                xmlSerializer.endTag("","lesson");
//                j++;
                temp += 10;
            }
            /*for (int j = 0; j < data.size(); j++) {

            }*/


            xmlSerializer.endTag("", "student");

        }

        /*xmlSerializer.startTag("", "something");
        xmlSerializer.attribute("", "ID", "000001");

        xmlSerializer.startTag("", "name");
        xmlSerializer.text("CO");
        xmlSerializer.endTag("", "name");

        xmlSerializer.endTag("", "something");*/



        //end tag <file>
        xmlSerializer.endTag("", "group");
        xmlSerializer.endTag("", "file");
        xmlSerializer.endDocument();


        } catch (IOException e) {
            e.printStackTrace();
        }
        return writer.toString();

    }
}

