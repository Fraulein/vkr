package com.example.myapplication5.app;


        import android.app.Activity;
        import android.app.ListActivity;
        import android.app.LoaderManager;
        import android.content.Context;
        import android.content.CursorLoader;
        import android.content.Loader;
        import android.database.Cursor;
        import android.os.Bundle;
        import android.util.Log;
        import android.view.ContextMenu;
        import android.view.Menu;
        import android.view.MenuItem;
        import android.view.View;
        import android.widget.AdapterView;
        import android.widget.ListView;
        import android.widget.SimpleCursorAdapter;
        import android.widget.TextView;

        import java.sql.SQLException;

public class Exmpl extends Activity implements LoaderManager.LoaderCallbacks<Cursor> {

    //private SQLAdapter dbHelper;
    //private Cursor tmpCursor;
    //String tmpStr = "Проверка!";
    private static final int CM_DELETE_ID = 1;
    SQLAdapter dbHelper;
    SimpleCursorAdapter tmpListAdapter;
    ListView tmpListView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new SQLAdapter(this);
        try {
            dbHelper.open();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

       /* dbHelper.createItem("Вадим","Тильзо","МСИА");
        dbHelper.createItem("Евгений","Сыпин","МСИА");
        dbHelper.createItem("Сергей","Терентьев","МСИА");
        dbHelper.createItem("Алексей","Тютякин","МСИА");*/

        /*TextView tmpText = (TextView) findViewById(R.id.textView);
        tmpCursor = dbHelper.fetchAllItem();*/

        /*tmpCursor.moveToFirst();
        tmpStr = tmpCursor.getString(tmpCursor.getColumnIndex(SQLAdapter.KEY_SURNAME));
        tmpText.setText(tmpStr);*/

        //dbHelper.close();

        tmpListView = (ListView) findViewById(R.id.lvActionMode);

        String[] fromColumn = new String[] { SQLAdapter.KEY_SURNAME};
        int[] toView = new int[] { android.R.id.text1};
        tmpListAdapter = new SimpleCursorAdapter(this,android.R.layout.simple_list_item_1, null, fromColumn, toView, 0);
        tmpListView.setAdapter(tmpListAdapter);
        Log.v("LoaderManager", "Инициализация Loader");
        getLoaderManager().initLoader(0, null, this);

        registerForContextMenu(tmpListView);

        //Log.v("LoaderManager", "Force Loader");
        //getLoaderManager().getLoader(0).forceLoad();
    }

    //ну это просто добавление по кнопке, но по хорошему надо добавлять откуда-нибудь
    public void onButtonClick(View view) {
        // добавляем запись
        dbHelper.createItem("TEXT+"+(tmpListAdapter.getCount() + 1),"TEST", "TEST");
        // получаем новый курсор с данными
        getLoaderManager().getLoader(0).forceLoad();
    }

    //создание контекстного меню, надо разбираться, что бы делать меню с интентами и прочими штуками
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, CM_DELETE_ID, 0, R.string.delete_record);
    }

    //выбор контекстного меню
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getItemId() == CM_DELETE_ID) {
            // получаем из пункта контекстного меню данные по пункту списка, надо уточнять как работает
            AdapterView.AdapterContextMenuInfo acmi = (AdapterView.AdapterContextMenuInfo) item
                    .getMenuInfo();
            // извлекаем id записи и удаляем соответствующую запись в БД
            dbHelper.deleteItem(acmi.id) ;
            // получаем новый курсор с данными
            getLoaderManager().getLoader(0).forceLoad();
            return true;
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public android.content.Loader<Cursor> onCreateLoader(int i, Bundle bundle) {

        SQLAdapter dbHelper = new SQLAdapter(this);
        try {
            dbHelper.open();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        Log.v("LoaderManager", "Создание AppCursorLoader");
        return new AppCursorLoader(this, dbHelper);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        Log.v("LoaderManager", "Завершение загрузки Cursor");
        tmpListAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {
        tmpListAdapter.swapCursor(null);
    }

    public static class AppCursorLoader extends CursorLoader {

        SQLAdapter dbHelper;

        public AppCursorLoader(Context context, SQLAdapter dbHelper) {
            super(context);
            this.dbHelper = dbHelper;
        }

        public Cursor loadInBackground() {
            Log.v("LoaderManager", "Start fetchAllItem");
            Cursor cursor = dbHelper.fetchAllItem();
            return cursor;
        }
    }
}


